﻿using RabbitMQ.Client;
using System;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;

class Send
{

    public static void Main()
    {
        var uri = new Uri("amqps://kehjivmj:KHTZflO2uOuv5vQ1xS3o7E_NVE2RbK68@lion.rmq.cloudamqp.com/kehjivmj");
        var factory = new ConnectionFactory();
        factory.Uri= uri;
        
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            bool ipChanged = false;
            String hostname = Dns.GetHostName();
            Host savedHostInfo, curentHostInfo = new Host { Name = hostname, Date = DateTime.Now, Address = GetIPAddress()};
            
            if (File.Exists(@"" + hostname + ".txt"))
            {
                savedHostInfo = RestoreHostData(curentHostInfo.Name);
                if (curentHostInfo.Address != savedHostInfo.Address)
                {
                    ipChanged = true;
                    SaveHostData(curentHostInfo);
                }
         
            }
            else
            {
                // if file doesn't exist, assume ip changed
                ipChanged = true;
            }
            
            if (ipChanged)
            {
                string message = JsonConvert.SerializeObject(curentHostInfo);
                var body = Encoding.UTF8.GetBytes(message);

                channel.QueueDeclare(queue: "bricip", durable: false, exclusive: false, autoDelete: false, arguments: null);
                channel.BasicPublish(exchange: "", routingKey: "bricip", basicProperties: null, body: body);
                Console.WriteLine("Sent {0}", message); 

                SaveHostData(curentHostInfo);
            }
                               
        }

    }

    private static Host RestoreHostData(String hostName)
    {
        string hostInJson, fileName = hostName + ".txt";
        Host restoreHost = null;
        try 
        {
            hostInJson = System.IO.File.ReadAllText(@"" + fileName); 
            restoreHost = JsonConvert.DeserializeObject<Host>(hostInJson);   
        }
        catch (FileNotFoundException e)
        {
            Console.WriteLine(e.Message);
        }
           
        return restoreHost;
    }

    static void SaveHostData(Host host) 
    {
        try
        {
            System.IO.File.WriteAllText(@""+ host.Name + ".txt", JsonConvert.SerializeObject(host));
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);     
        }
    }
    static string GetIPAddress()
    {
        String address = "";
        WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
        using (WebResponse response = request.GetResponse())
        using (StreamReader stream = new StreamReader(response.GetResponseStream()))
        {
            address = stream.ReadToEnd();
        }

        //Search for the ip in the html
        int first = address.IndexOf("Address: ") + 9;
        int last = address.LastIndexOf("</body>");
        address = address.Substring(first, last - first);

        return address;
    }
    
}

internal class Host
{

    public string Name { get;  set; }
    public DateTime Date { get;  set; }
    public string Address { get;  set; }

}